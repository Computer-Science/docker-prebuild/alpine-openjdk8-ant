FROM openjdk:8-jdk-alpine

RUN apk add --no-cache curl tar bash

ARG ANT_VERSION=1.9.9
ARG SHA=38e808e9d6710db600f71f58c401936d95feead2
ARG BASE_URL=https://www.apache.org/dist/ant/binaries/apache-ant-${ANT_VERSION}-bin.tar.gz

RUN curl -fsSL -o /tmp/apache-ant.tar.gz ${BASE_URL} \
  && echo "${SHA}  /tmp/apache-ant.tar.gz" | sha1sum -c - \
  && mkdir /usr/share/ant \
  && tar -xzf /tmp/apache-ant.tar.gz -C /usr/share/ant --strip-components=1 \
  && rm -f /tmp/apache-ant.tar.gz \
  && ln -s /usr/share/ant/bin/ant /usr/bin/ant

CMD [ "ant" ]